def powLogN(num, power):
    if(power == 1):
        return num
    else:
        mid = power / 2
        prev = powFoo(num, mid)

    if(power % 2 == 1):
        return prev * prev * num
    else:
        return prev * prev


def diffAP(lst):
    a = lst[0]
    n = len(lst)
    return (2 * sum(lst) - 2 * a * n )    /  (n * (n - 1) )

import sys
import math

def findMissingNum(lst):
    d = sys.maxsize

    def helper(newlst):
        nonlocal d
       
##        print("val", newlst)
##        print("d", d)
        if len(newlst) == 0 or len(newlst) == 1:
            return -1
        elif len(newlst) == 2:
            if(newlst[1] - newlst[0] == diffAP(lst) ):
                return -1
            else:
                return newlst[0] + d
       
        else:
            listLength = len(newlst)
            if(len(newlst) % 2 == 0):
                #leftList = newlst[0: math.floor(len(newlst)/2)]
                #rightList = newlst[ math.ceil(len(newlst)/2): len(newlst)]
           
                leftList = newlst[: listLength//2]
                rightList = newlst[ intlistLength//2  :]
            else:
                leftList = newlst[0: math.ceil(len(newlst)/2)]
                rightList = newlst[ math.floor(len(newlst)/2)  : listLength]
               
##            print("leftList", leftList)
##            print("rightList", rightList)
           
           
            if(leftList[-1] - leftList[0] > rightList[-1] - rightList[0] ):
                d = diffAP(rightList)
                return helper(leftList)
            elif(leftList[-1] - leftList[0] < rightList[-1] - rightList[0] ):
                d = diffAP(leftList)
                return helper(rightList)
            else:
                return -1
           
               
               
    return helper(lst)


import random
def generateXYCoords(length):
    x = [random.randrange(0, 20) for _ in range(length)]
    y = [random.randrange(0, 20) for _ in range(length)]
    lst = zip(x,y)
    #print(tuple(zip(x,y)))
    return zip(x,y)


#mergeSort(list(generateXYCoords(6)))
def mergeSort(newlst):
   
    def orderPointsNLogN(lst):
        if len(lst) > 1:
            mid = int(len(lst) /2 )
           
           
            left = lst[:mid]
            right = lst[mid:]

            orderPointsNLogN(left)
            orderPointsNLogN(right)

            i = j = k = 0

            while i < len(left) and j < len(right):
                if(left[i] < right[j]):
                    lst[k] = left[i]
                    i += 1
                    k += 1
                else:
                    lst[k] = right[j]
                    j += 1
                    k += 1
            while i < len(left):
                lst[k] = left[i]
                i += 1
                k += 1
            while j < len(right):
                lst[k] = right[j]
                j += 1
                k += 1
        return newlst          
    return orderPointsNLogN(newlst)

#lst [(8, 2), (9, 9), (2, 4), (17, 2), (10, 14), (10, 19)]


def rightparetoPoint(lst):
    yCurrent = -sys.maxsize
    xCurrent = sys.maxsize
    newlst = []
   
    for index in reversed( range(len(lst) )):

##        print("lstx", lst[index])
##        print("x", lst[index][0])
##        print("y", lst[index][1])
##        print("newlst", newlst)
       
        x = lst[index][0]
        y = lst[index][1]

       
       
        if(y > yCurrent):
            if(len(newlst) > 0 ):
                if(x !=  newlst[-1][0]):
                    newlst += [(x,y)]
                    yCurrent = y
            else:
                newlst += [(x,y)]
                yCurrent = y
    return newlst
       
   
def paretoPoint():
    lst = list(generateXYCoords(6))
    lst = mergeSort(lst)
    #print("newlstnewlst", lst)
    return rightparetoPoint(lst)
